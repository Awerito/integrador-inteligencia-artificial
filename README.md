# Integrador Inteligencia Artificial

Repositorio para el proyecto integrador del ramo Inteligencia Artificial de Ingeniería
Civil Informática de la Universidad de Los Lagos año 2020.
Profesor Joel Torres Carrasco.

## Instrucciones de uso

Dependencias especificadas en `requirements.txt`

Para correr el juego:

    $ python3 main_game.py


## Informe

Informe pre-compilado en `informe/informe.pdf`, puede recompilarse desde el
`informe.tex`.

### Integrantes

- Diego Muñoz ([@Awerito](https://gitlab.com/Awerito)) 
- Cristian Oyarzo ([@ClownSK](https://gitlab.com/ClownSk)) 
- Victor Rodriguez ([@Pejelagartoh](https://gitlab.com/Pejelagartoh)) 
- Sebastian Vidal ([@Ezfeik](https://gitlab.com/Ezfeik)) 
