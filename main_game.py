import pygame
from player import Entity
from pygame.draw import line as draw_line
from pygame import mixer, image

# Global variables
UP =    ( 0, -1)
DOWN =  ( 0,  1)
LEFT =  (-1,  0)
RIGHT = ( 1,  0)
BG = (50,50,50)
P1_GREEN = (14, 134, 88)
P2_RED = (200, 22, 32)

# Game settings
scaling_factor = 6
S_WIDTH, S_HEIGHT = 1600, 900
FPS = 60
pygame.init()
screen = pygame.display.set_mode((S_WIDTH, S_HEIGHT), pygame.FULLSCREEN)
aux_surface = pygame.Surface((int(S_WIDTH/scaling_factor), int(S_HEIGHT/scaling_factor)))
cursor = pygame.mouse.set_visible(False)
clock = pygame.time.Clock()
done = False

# Music Settings
mixer.init()
mixer.music.load('music/dindindindara.mp3')
mixer.music.set_volume(0.1)
mixer.music.play()

# Playable area
width, height = S_WIDTH/scaling_factor, int(S_HEIGHT/scaling_factor * 0.92)

# Players init
first_player = Entity(width / 4, height / 2, P1_GREEN, RIGHT)
second_player = Entity(3 * width / 4, height / 2, P2_RED, LEFT)
first_player_score = 0
second_player_score = 0


def reset_game():

    first_player.reset(width / 4, height / 2, P1_GREEN, RIGHT)
    second_player.reset(3 * width / 4, height / 2, P2_RED, LEFT)


# Game interface
font = pygame.font.Font('freesansbold.ttf', 40)
font2 = pygame.font.Font('freesansbold.ttf', 60)
font3 = pygame.font.Font('freesansbold.ttf', 30)


def show_score(x, y):

    draw_line(
        screen, (255, 255, 255), (0, int(height*scaling_factor)),
        (int(width*scaling_factor), int(height*scaling_factor)),
        3
    )
    first_logo = image.load("img/bdbt.png")
    second_logo = image.load("img/perrito.png")
    first_score = font.render(str(first_player_score), True, first_player.color)
    second_score = font.render(str(second_player_score), True, second_player.color)
    text_height = int((S_HEIGHT - height*scaling_factor) / 2 + height*scaling_factor - 15)
    screen.blit(first_logo, (int(x * 0.15), text_height-10))
    screen.blit(second_logo, (int(x * 1.80), text_height-10))
    screen.blit(first_score, (int(x * 0.25), text_height))
    screen.blit(second_score, (int(x * 1.75), text_height))


# Game loop
while not done:
    dt = clock.tick(FPS)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                done = True

            if event.key == pygame.K_w:
                first_player.turn(UP)

            if event.key == pygame.K_s:
                first_player.turn(DOWN)

            if event.key == pygame.K_a:
                first_player.turn(LEFT)

            if event.key == pygame.K_d:
                first_player.turn(RIGHT)

            if event.key == pygame.K_UP:
                second_player.turn(UP)

            if event.key == pygame.K_DOWN:
                second_player.turn(DOWN)

            if event.key == pygame.K_LEFT:
                second_player.turn(LEFT)

            if event.key == pygame.K_RIGHT:
                second_player.turn(RIGHT)

    if first_player_score != 5 and second_player_score != 5:
        #Logic section
        first_player.move(dt)
        second_player.move(dt)

        collition_check = False

        first_wall_collide = first_player.collide(width, height)
        second_wall_collide = second_player.collide(width, height)

        first_collision = first_player.player_collide(second_player)
        second_collision = second_player.player_collide(first_player)

        if first_wall_collide:
            second_player_score += 1
            collition_check = True

        if second_wall_collide:
            first_player_score += 1
            collition_check = True

        if first_collision == 4 and second_collision == 2:
            first_player_score += 1
            collition_check = True
        elif second_collision == 4 and first_collision == 2:
            second_player_score += 1
            collition_check = True
        else:
            if first_collision:
                second_player_score += 1
                collition_check = True

            if second_collision:
                first_player_score += 1
                collition_check = True

        if collition_check:
            reset_game()
        #=============

        # Draw section
        aux_surface.fill(BG)
        first_player.draw(aux_surface)
        second_player.draw(aux_surface)
        screen.blit(pygame.transform.scale(aux_surface, screen.get_rect().size), (0, 0))
        show_score((width / 2)*scaling_factor, 720*scaling_factor)
        #=============

    else:
        if first_player_score == 5 and second_player_score == 5:
            sample_text = font2.render("EMPATE!", True, (255, 255, 255))
            screen.blit(sample_text, (int(S_WIDTH*0.4), int(S_HEIGHT*0.45)))
        elif first_player_score == 5:
            sample_text = font2.render("JUGADOR 1 GANA!", True, (255, 255, 255))
            screen.blit(sample_text, (int(S_WIDTH*0.31), int(S_HEIGHT*0.45)))
        elif second_player_score == 5:
            sample_text = font2.render("JUGADOR 2 GANA!", True, (255, 255, 255))
            screen.blit(sample_text, (int(S_WIDTH*0.31), int(S_HEIGHT*0.45)))

        sample_text2 = font3.render('Presione "ESC" para Salir', True, (255, 255, 255))
        screen.blit(sample_text2, (int(S_WIDTH*0.37), int(S_HEIGHT*0.55)))

    pygame.display.update()
