from pygame.draw import line as draw_line


class Entity:

    def __init__(self, x, y, color, direction):

        self.x = x
        self.y = y
        self.real_x = x
        self.real_y = y
        self.x_vel = 0.04
        self.y_vel = 0.04
        self.color = color
        self.alive = True
        self.direction = direction
        self.lines = [(self.x, self.y)]


    def reset(self, x, y, color, direction):

        self.__init__(x, y, color, direction)


    def get_lines(self): return self.lines


    def get_pos(self): return (self.x, self.y)


    def draw(self, screen):

        if self.alive:
            if len(self.lines)>1:
                for p1, p2 in zip(self.lines[:-1], self.lines[1:]):
                    draw_line(screen, self.color, p1, p2, 1)

            draw_line(screen, self.color, self.lines[-1], (self.x, self.y), 1)


    def move(self, dt):

        self.real_x += self.x_vel * self.direction[0] * dt
        self.real_y += self.y_vel * self.direction[1] * dt
        self.x = round(self.real_x)
        self.y = round(self.real_y)

    def turn(self, d):

        if not ((d[0] * (-1) == self.direction[0] and d[1] * (-1) == self.direction[1]) or d == self.direction):
            self.real_x, self.real_y = self.x, self.y

            self.direction = d
            self.lines.append((int(self.x), int(self.y)))


    def collide(self, width, height):

        if (self.x <= 0 or self.x >= width or
                self.y <= 0 or self.y >= height):
            self.alive = False
            # Put audio here
            return True

        if len(self.lines) > 1:
            for p1, p2 in zip(self.lines[:-2], self.lines[1:-2]):
                d = (self.x - self.lines[-1][0]) * (p1[1] - p2[1]) - \
                    (self.y - self.lines[-1][1]) * (p1[0] - p2[0])
                if not d == 0:
                    t = ((self.x - p1[0]) * (p1[1] - p2[1]) - \
                         (self.y - p1[1]) * (p1[0] - p2[0])) / d
                    u = ((self.x - self.lines[-1][0]) * (self.y - p1[1]) - \
                         (self.y - self.lines[-1][1]) * (self.x - p1[0])) / d * (-1)
                    if 0 <= t <= 1 and 0 <= u <= 1:
                        self.alive = False
                        # Put audio here
                        return True


    def player_collide(self, player):

        player_lines = player.get_lines()
        init_pos = player.get_pos()
        end_pos = player_lines[-1]

        if (self.x, self.y) == init_pos:
            self.alive = False
            # Tie/Do not put collision audio
            return 1

        if (self.x, self.y) in player_lines:
            self.alive = False
            # Vertex
            # Put audio here
            return 2

        if len(player_lines) > 1:
            for p1, p2 in zip(player_lines[:-1], player_lines[1:]):
                d = (self.x - self.lines[-1][0]) * (p1[1] - p2[1]) - \
                    (self.y - self.lines[-1][1]) * (p1[0] - p2[0])
                if not d == 0:
                    t = ((self.x - p1[0]) * (p1[1] - p2[1]) - \
                         (self.y - p1[1]) * (p1[0] - p2[0])) / d
                    u = ((self.x - self.lines[-1][0]) * (self.y - p1[1]) - \
                         (self.y - self.lines[-1][1]) * (self.x - p1[0])) / d * (-1)
                    if 0 <= t <= 1 and 0 <= u <= 1:
                        self.alive = False
                        # Line
                        # Put audio here
                        return 3

        d = (self.x - self.lines[-1][0]) * (init_pos[1] - end_pos[1]) - \
            (self.y - self.lines[-1][1]) * (init_pos[0] - end_pos[0])
        if not d == 0:
            t = ((self.x - init_pos[0]) * (init_pos[1] - end_pos[1]) - \
                 (self.y - init_pos[1]) * (init_pos[0] - end_pos[0])) / d
            u = ((self.x - self.lines[-1][0]) * (self.y - init_pos[1]) - \
                 (self.y - self.lines[-1][1]) * (self.x - init_pos[0])) / d * (-1)
            if 0 <= t <= 1 and 0 <= u <= 1:
                if self.x == self.lines[-1][0]:
                    if (self.y == init_pos[1] == end_pos[1] or
                            self.lines[-1][1] == init_pos[1] == end_pos[1]):
                        self.alive = False
                        # Put audio here
                        return 4
                else:
                    if (self.x == init_pos[0] == end_pos[0] or
                            self.lines[-1][0] == init_pos[0] == end_pos[0]):
                        self.alive = False
                        # Put audio here
                        return 4
        return 0
